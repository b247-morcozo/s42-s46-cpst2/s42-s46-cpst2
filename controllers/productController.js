const Product = require("../models/Product");

module.exports.addProduct = (reqBody) => {

	//if(Data.isAdmin){
	  

		let newProduct = new Product({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return {
					message: "New product successfully created!"
				}
			};
		});

	};

/*	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};*/

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result
	});
};

module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result
	});
};

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((course, error) => {

		if(error){
			return false
		} else {
			let message = `Successfully updated Product - "${reqParams.id}"`

			return message;
		}
	});
};

module.exports.archiveProduct = (reqParams, reqBody) => {

	let archivedProduct = {
		isActive : reqBody.isActive
	};


	return Product.findByIdAndUpdate(reqParams.id, archivedProduct).then((course,error) => {
		
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.activateProduct = (reqParams, reqBody) => {

	let activateProduct = {
		isActive : reqBody.isActive
	};


	return Product.findByIdAndUpdate(reqParams.id, activateProduct).then((course,error) => {
		
		if(error){
			return false
		} else {
			return true
		}
	})
}