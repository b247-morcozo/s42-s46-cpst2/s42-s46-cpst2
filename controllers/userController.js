const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Product = require("../models/Product")

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return false;
		
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect){
				
				return { access : auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (userdata) => {
	
	return User.findOne({id: userdata._id}).then(result => {
		if(result == null){
			return false
		} else {
			result.password = ""
			return result
		}
	});
};


module.exports.checkout = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orderedProduct.push({
			productId: data.productId
			//productName: data.productName,
			//quantity: data.quantity,
			//totalAmount: data.totalAmount
		});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({userId : data.userId });

		return product.save().then((product, error) => {
			if (error){
				return false
			} else {
				return true
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return true;
	} else {
		return false;
	};
};

