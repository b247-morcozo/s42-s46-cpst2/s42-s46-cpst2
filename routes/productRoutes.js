const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

// Route for creating a product
//router.post("/add", auth.verify, (req, res) => {
  router.post("/add", (req, res) => {

	/*const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}*/
	
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
});

//route for retrieving all products
router.get("/all", (req, res) => {

	isAdmin: auth.decode(req.headers.authorization).isAdmin
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
})


//Route for retrieving all active products
router.get("/active",(req,res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific product
router.get("/:productId/details", (req,res) => {

	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//Route for updating a product
router.put("/:id/update", auth.verify, (req,res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for archiving a product
router.patch("/:id/archive", auth.verify, (req,res) => {

	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for activating a product
router.patch("/:id/activate", auth.verify, (req,res) => {

	productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;